<?php

/**
 * @file
 * Contains req_course.page.inc.
 *
 * Page callback for Required Course Connector entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Required Course Connector templates.
 *
 * Default template: req_course.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_req_course(array &$variables) {
  // Fetch ReqCourse Entity Object.
  $req_course = $variables['elements']['#req_course'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
