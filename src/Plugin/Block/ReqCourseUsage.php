<?php

namespace Drupal\hfc_req_course\Plugin\Block;

use Drupal\hfc_req_course\ReqCourseInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ReqCourseUsage' block.
 *
 * @Block(
 *  id = "req_course_usage",
 *  admin_label = @Translation("Required Course Connector Usage"),
 * )
 */
class ReqCourseUsage extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Stores the Current Route Match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * Constructs a new ReqCourseUsage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The Current Route Match service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RouteMatchInterface $route_match
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * Gets the current rqc entity, if any.
   */
  protected function getReqCourse() {
    $rqc = $this->routeMatch->getParameter('req_course');
    if ($rqc instanceof ReqCourseInterface) {
      return $rqc;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($rqc = $this->getReqCourse()) {
      $build = [];

      $query = $this->entityTypeManager->getStorage('node')->getQuery()
        ->accessCheck(FALSE);

      $rqcfields = $query->orConditionGroup()
        ->condition('field_program_admission_list', $rqc->id())
        ->condition('field_program_rqcor_list', $rqc->id())
        ->condition('field_program_rqsup_list', $rqc->id())
        ->condition('field_pseudo_courses', $rqc->id())
        ->condition('field_crs_xref', $rqc->id());

      $query->condition($rqcfields);

      // @todo Neither version of this filtering is working right.
      // I needed to get this working and move on, so I moved it to the display loop.

      // $cpfields = $query->andConditionGroup()
      //   ->condition('type', 'catalog_program')
      //   ->condition('field_program_status', 'inactive', '<>');

      // $pmfields = $query->andConditionGroup()
      //   ->condition('type', 'program_master')
      //   ->condition('field_program_status', 'inactive', '<>');

      // $ppfields = $query->andConditionGroup()
      //   ->condition('type', 'program_proposal')
      //   ->condition('field_proposal_processed', 1, '<>');

      // $ctfields = $query->orConditionGroup()
      //   ->condition($cpfields)
      //   ->condition($pmfields)
      //   ->condition($ppfields)
      //   ->condition('type', 'pseudo_course');

      // $query->condition($ctfields);

      // $query->notExists('field_deactivation_date');
      // $query->notExists('field_proposal_processed');
      // $query->condition('field_inactive', 1, '<>');

      $query->sort('type');
      $query->sort('title');

      $result = $query->execute();

      if (!empty($result)) {
        $nodeStorage = $this->entityTypeManager->getStorage('node');
        $items = [];
        foreach ($result as $nid) {
          $node = $nodeStorage->load($nid);
          // @todo This check is needed until the query filtering can be fixed above.
          if (
            !($node->getType() == 'program_proposal' && $node->field_proposal_processed->value == 1) &&
            !($node->getType() == 'program_master' && !empty($node->field_deactivation_date->value)) &&
            !($node->getType() == 'catalog_program' && $node->field_program_status->value == 'inactive')
          ) {
            $items[] = $node->toLink();
          }
        }
        if (!empty($items)) {
          $build['req_course_usage'] = [
            '#theme' => 'item_list',
            '#items' => $items,
          ];
        }
      }

      // @todo I can't get this to be stable. If I try to implement any useful caching,
      // then the block just goes blank on every page if a related node changes.
      // @see https://www.drupal.org/developing/api/8/cache/contexts
      // @see https://www.drupal.org/developing/api/8/cache/tags
      // @see https://www.drupal.org/docs/8/api/cache-api/cache-max-age
      // $build['#cache']['contexts'] = ['url'];
      // $build['#cache']['tags'] = ['req_course:' . $rqc->id(), 'node_list'];
      $build['#cache']['max-age'] = 0;

      return $build;
    }
  }

}
