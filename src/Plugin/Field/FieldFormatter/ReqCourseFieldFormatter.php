<?php

namespace Drupal\hfc_req_course\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for Required Course Connector field formatter.
 *
 * @FieldFormatter(
 *   id = "hfc_req_course_field_formatter",
 *   label = @Translation("Required Course Connector"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ReqCourseFieldFormatter extends EntityReferenceFormatterBase {

  use LoggerChannelTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a ReqCourseFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Determine which related node to link.
   */
  protected static function courseLinkModes() {
    return [
      'course_master' => t('Course Master (or Proposal)'),
      'catalog_course' => t('Catalog Course'),
      'catalog_archive' => t('Catalog Archive'),
    ];
  }

  /**
   * Build a list of available entity view modes.
   */
  protected static function nodeViewModes() {
    static $modes;
    if (!empty($modes)) {
      return $modes;
    }

    $modes = [];
    $view_modes = \Drupal::service('entity_display.repository')->getViewModes('node');

    foreach ($view_modes as $id => $mode) {
      $modes[$id] = $mode['label'];
    }

    return $modes;
  }

  /**
   * Build a list of table modes for display.
   */
  protected static function tableModes() {
    return [
      0 => t('Links only'),
      1 => t('Table display'),
      2 => t('Unformatted'),
      3 => t('Total Hours Only'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'view_mode' => 'course_master',
      'pseudo_course_display' => 'full',
      'table_mode' => 1,
      'display_total' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['table_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Display format'),
      '#options' => $this->tableModes(),
      '#default_value' => $this->getSetting('table_mode'),
      '#description' => $this->t('Select field output display format.'),
      '#weight' => 0,
    ];

    $elements['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Course link mode'),
      '#options' => $this->courseLinkModes(),
      '#default_value' => $this->getSetting('view_mode'),
      '#description' => $this->t('Select which required course entity to display with this field.<br><em>(Automatically includes all pseudo-courses.)</em>'),
      '#weight' => 0,
    ];

    $elements['pseudo_course_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Pseudo Course display mode'),
      '#options' => $this->nodeViewModes(),
      '#default_value' => $this->getSetting('pseudo_course_display'),
      '#description' => $this->t('Select which view mode generates nested pseudo course descriptions.'),
      '#weight' => 0,
    ];

    $elements['display_total'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display total credit hours'),
      '#options' => [1 => $this->t('Yes'), 0 => $this->t('No')],
      '#default_value' => $this->getSetting('display_total'),
      '#description' => $this->t('Include total credit hours in display.'),
      '#required' => TRUE,
      '#weight' => 0,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Display mode: %m', ['%m' => $this->tableModes()[$this->getSetting('table_mode')]]);
    if ($mode = $this->getSetting('view_mode')) {
      $mode = !empty($this->courseLinkModes()[$mode]) ? $this->courseLinkModes()[$mode] : $mode;
      $summary[] = $this->t('Course Link: %m', ['%m' => $mode]);
    }
    if ($mode = $this->getSetting('pseudo_course_display')) {
      $mode = !empty($this->nodeViewModes()[$mode]) ? $this->nodeViewModes()[$mode] : $mode;
      $summary[] = $this->t('Nested Pseudo Courses: %m', ['%m' => $mode]);
    }
    $display_total = $this->getSetting('display_total') ? 'Yes' : 'No';
    $summary[] = $this->t('Include total hours: %m', ['%m' => $display_total]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @see EntityReferenceFormatterBase::getEntitiesToView()
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {

    // Retrieves an array of ReqCourse entities to render.
    $entities = $this->getEntitiesToView($items, $langcode);

    if ($this->getSetting('table_mode') == 3) {
      return !empty($entities) ? $this->viewTotalHoursOnly($entities) : [];
    }
    else {
      return !empty($entities) ? $this->viewFullContents($entities) : [];
    }
  }

  /**
   * Render the full contents of the required course connector field.
   */
  private function viewFullContents($entities) {

    // Initialize output.
    $elements = [];

    $view_mode = $this->getSetting('view_mode');

    // Initialize $rows for tables, or $linklist for item list.
    $rows = [];
    $linklist = [];
    $hours = 0;

    foreach ($entities as $entity) {

      // Get the appropriate node.
      switch ($view_mode) {
        case 'course_master':
          $node = $entity->getMaster();
          if (empty($node)) {
            $node = $entity->getProposal();
          }
          if (empty($node)) {
            $this->messenger()->addWarning($this->t('Could not display link to required course @t', ['@t' => $entity->label()]));
            $this->getLogger('hfc_req_course')->error('Could not display link to required course @t', ['@t' => $entity->label()]);
          }
          break;

        case 'catalog_course':
        case 'catalog_archive':
          $node = $entity->getCatalog();
          if (empty($node)) {
            $this->messenger()->addWarning($this->t('Could not display link to required course @t', ['@t' => $entity->label()]));
            $this->getLogger('hfc_req_course')->error('Could not display link to required course @t', ['@t' => $entity->label()]);
          }
          break;
      }

      if (!empty($node)) {
        if ($node->getType() == 'pseudo_course') {
          $description = $this->entityTypeManager->getViewBuilder('node')->view($node, $this->getSetting('pseudo_course_display'));
        }
        elseif ($view_mode == 'catalog_archive') {
          $name = mb_strtolower($node->field_crs_subject->target_id . '-' . $node->field_crs_number->value);
          $description = $this->t(
            '<a href="../courses/@name">@label</a>',
            ['@name' => $name, '@label' => $entity->label()]
          );
        }
        else {
          $description = $node->toLink()->toString();
        }
      }
      else {
        $description = $entity->label();
      }
      $rows[] = [
        ['data' => $description, 'class' => ['course-description']],
        ['data' => $entity->getCreditHours(2), 'class' => ['credit-hours']],
      ];
      $linklist[] = $description;
      $hours += $entity->credit_hours->value;
    }

    switch ($this->getSetting('table_mode')) {
      case 0:
        if (!empty($linklist)) {
          $elements[] = [
            '#theme' => 'item_list',
            '#items' => $linklist,
          ];
        }
        break;

      case 1:
        if (!empty($rows)) {
          $elements[] = [
            '#type' => 'table',
            '#header' => [$this->t('Course name'), $this->t('Credit Hours')],
            '#rows' => $rows,
            '#attributes' => ['class' => ['rqc-full-display']],
          ];
        }
        break;

      case 2:
        foreach ($linklist as $link) {
          $elements[] = [
            '#markup' => $link,
          ];
        }
        break;
    }

    if ($this->getSetting('display_total')) {
      $elements[] = [
        '#markup' => $this->t(
          '<strong>Credit Hours:</strong> @h',
          ['@h' => round($hours, 2)]
        ),
      ];
    }
    return $elements;
  }

  /**
   * Just total the credit hours and return nothing else.
   *
   * This is for use in views.
   */
  private function viewTotalHoursOnly($entities) {
    $hours = 0;
    foreach ($entities as $entity) {
      $hours += $entity->credit_hours->value;
    }
    return [['#markup' => number_format($hours, 2)]];
  }

}
