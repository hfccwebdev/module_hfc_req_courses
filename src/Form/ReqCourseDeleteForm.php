<?php

namespace Drupal\hfc_req_course\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Required Course Connector entities.
 *
 * @ingroup hfc_req_coursee
 */
class ReqCourseDeleteForm extends ContentEntityDeleteForm {

}
