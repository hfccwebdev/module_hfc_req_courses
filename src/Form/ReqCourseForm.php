<?php

namespace Drupal\hfc_req_course\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Required Course Connector edit forms.
 *
 * @ingroup hfc_req_course
 */
class ReqCourseForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t(
          'Created the %label Required Course Connector.',
          ['%label' => $entity->label()]
        ));
        break;

      default:
        $this->messenger()->addMessage($this->t(
          'Saved the %label Required Course Connector.',
          ['%label' => $entity->label()]
        ));
    }
    $form_state->setRedirect(
      'entity.req_course.canonical',
      ['req_course' => $entity->id()]
    );
  }

}
