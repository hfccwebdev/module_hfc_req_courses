<?php

namespace Drupal\hfc_req_course\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Bulk Update Form.
 *
 * @package Drupal\hfc_req_course\Form
 */
class ReqCourseBulkUpdateForm extends FormBase {

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('entity_type.manager')
    );
  }

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'req_course_bulk_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['message'] = [
      '#prefix' => '<p>',
      '#markup' => $this->t('This process will update all Required Course Connectors.'),
      '#suffix' => '</p>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update Connectors'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $nids = [];

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $types = $query->orConditionGroup()
      ->condition('type', 'course_master')
      ->condition('type', 'pseudo_course');
    $masters = $query
      ->condition($types)
      ->sort('title')
      ->accessCheck(FALSE)
      ->execute();

    foreach ($masters as $nid) {
      $nids[] = $nid;
    }

    $proposals = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'course_proposal')
      ->notExists('field_course_master')
      ->condition('field_proposal_processed', 0)
      ->sort('title')
      ->accessCheck(FALSE)
      ->execute();

    foreach ($proposals as $nid) {
      $nids[] = $nid;
    }

    $batch = [
      'title' => $this->t('Updating Required Course Connectors...'),
      'operations' => [
        ['\Drupal\hfc_req_course\ReqCourseSync::checkBroken', []],
        ['\Drupal\hfc_req_course\ReqCourseSync::bulkUpdate', [$nids]],
      ],
      'finished' => '\Drupal\hfc_req_course\ReqCourseSync::bulkUpdateFinishedCallback',
    ];
    batch_set($batch);
  }

}
