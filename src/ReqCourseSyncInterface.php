<?php

namespace Drupal\hfc_req_course;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides synchronization services for Required Course entities.
 */
interface ReqCourseSyncInterface {

  /**
   * Update hfc_req_course entity from node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The entity object.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function update(EntityInterface $node);

  /**
   * Update or remove hfc_req_course entity when node is deleted.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The entity object.
   */
  public function nodeDelete(EntityInterface $node);

  /**
   * Check for broken req course entities.
   *
   * @param string[] $context
   *   Batch operations context variables.
   */
  public static function checkBroken(array &$context);

  /**
   * Bulk update all hfc_req_course entities by nodes.
   *
   * @param int[] $nids
   *   An array of nids to check.
   * @param string[] $context
   *   Batch operations context variables.
   */
  public static function bulkUpdate(array $nids, array &$context);

  /**
   * Callback when connector bulk update batch operation is finished.
   */
  public static function bulkUpdateFinishedCallback($success, $results, $operations);

}
