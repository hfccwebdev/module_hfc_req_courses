<?php

namespace Drupal\hfc_req_course;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Required Course Connector entities.
 *
 * @ingroup hfc_req_course
 */
interface ReqCourseInterface extends ContentEntityInterface {

  /**
   * Get hfc_req_course entity by Node ID.
   *
   * @param string $field
   *   Entity field to match.
   * @param mixed $nid
   *   Value to search.
   *
   * @return ReqCourseInterface
   *   The selected required course connector
   */
  public static function lookup($field, $nid);

  /**
   * Gets the Required Course Connector name.
   *
   * @return string
   *   Name of the Required Course Connector.
   */
  public function getName();

  /**
   * Sets the Required Course Connector name.
   *
   * @param string $name
   *   The Required Course Connector name.
   */
  public function setName($name);

  /**
   * Gets the current Course Proposal.
   *
   * @return \Drupal\node\NodeInterface
   *   The current Course Proposal node.
   */
  public function getProposal();

  /**
   * Gets the current Course Proposal Node ID.
   *
   * @return int
   *   The current Course Proposal nid.
   */
  public function getProposalNid();

  /**
   * Sets the Course Proposal Node ID.
   *
   * @param int $nid
   *   The node id.
   */
  public function setProposal($nid);

  /**
   * Gets the current Course Master.
   *
   * @return \Drupal\node\NodeInterface
   *   The current Course Master node.
   */
  public function getMaster();

  /**
   * Gets the current Course Master Node ID.
   *
   * @return int
   *   The current Course Master nid.
   */
  public function getMasterNid();

  /**
   * Sets the Course Master Node ID.
   *
   * @param int $nid
   *   The node id.
   */
  public function setMaster($nid);

  /**
   * Gets the current Supplemental Course Info.
   *
   * @return \Drupal\node\NodeInterface
   *   The current Supplemental Course Info node.
   */
  public function getSupp();

  /**
   * Gets the current Supplemental Course Info Node ID.
   *
   * @return int
   *   The current Supplemental Course Info nid.
   */
  public function getSuppNid();

  /**
   * Sets the Supplemental Course Info Node ID.
   *
   * @param int $nid
   *   The node id.
   */
  public function setSupp($nid);

  /**
   * Gets the current Catalog Course.
   *
   * @return \Drupal\node\NodeInterface
   *   The current Catalog Course node.
   */
  public function getCatalog();

  /**
   * Gets the current Catalog Course Node ID.
   *
   * @return int
   *   The current Catalog Course nid.
   */
  public function getCatalogNid();

  /**
   * Sets the Catalog Course Node ID.
   *
   * @param int $nid
   *   The node id.
   */
  public function setCatalog($nid);

  /**
   * Gets the number of credit hours.
   *
   * @param int $scale
   *   Number of decimal places to display.
   *
   * @return float
   *   The course credit hours.
   */
  public function getCreditHours($scale);

  /**
   * Sets the number of credit hours.
   *
   * @param float $credit_hours
   *   The number of credit hours.
   */
  public function setCreditHours($credit_hours);

  /**
   * Gets the inactive flag.
   *
   * @return bool
   *   Returns TRUE if inactive.
   */
  public function getInactive();

  /**
   * Sets the inactive flag.
   *
   * @param bool $inactive
   *   The flag value.
   */
  public function setInactive($inactive);

}
