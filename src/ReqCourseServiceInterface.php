<?php

namespace Drupal\hfc_req_course;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the Required Course Connector Service Interface.
 */
interface ReqCourseServiceInterface {

  /**
   * Set values for Required Course Connector based on source node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The source node.
   *
   * @return array
   *   An array of values.
   */
  public function getConnectorValues(EntityInterface $node);

}
