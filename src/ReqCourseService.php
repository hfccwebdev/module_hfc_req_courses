<?php

namespace Drupal\hfc_req_course;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;

/**
 * Defines the Required Course Connector Service.
 */
class ReqCourseService implements ReqCourseServiceInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Stores the database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Stores the Catalog Helper Service.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Constructs a new AssessmentReportTools object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The Database connection.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $helper
   *   The Catalog Helper service.
   */
  public function __construct(
    Connection $database,
    CatalogUtilitiesInterface $helper
  ) {
    $this->database = $database;
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectorValues(EntityInterface $node) {

    switch ($node->getType()) {
      case 'course_master':
        $title = $node->label();
        $proposal = $this->helper->getActiveCourseProposal($node->id());
        $master = $node;
        $supplemental = $this->helper->getSupplementalCourseInfo($node->id());
        $catalog = $this->helper->getCatalogCourse($node->id());
        $credit_hours = $node->field_crs_credit_hours->value;
        $inactive = $node->field_inactive->value;
        break;

      case 'pseudo_course':
        $title = $node->label();
        $proposal = $node;
        $master = $node;
        $supplemental = NULL;
        $catalog = $node;
        $credit_hours = $node->field_crs_credit_hours->value;
        $inactive = $node->field_inactive->value;
        break;

      case 'course_proposal':
        if ($master = $node->field_course_master->entity) {
          $title = $master->label();
          $proposal = $node;
          $catalog = $this->helper->getCatalogCourse($master->id());
          $supplemental = $this->helper->getSupplementalCourseInfo($master->id());
          $credit_hours = $master->field_crs_credit_hours->value;
          $inactive = $master->field_inactive->value;
        }
        else {
          $title = $node->label();
          $proposal = $node;
          $master = NULL;
          $supplemental = NULL;
          $catalog = NULL;
          $credit_hours = $node->field_crs_credit_hours->value;
          $inactive = FALSE;
        }
        break;

      case 'supplemental_course_info':
        if ($master = $node->field_course_master->entity) {
          $title = $master->label();
          $proposal = $this->helper->getActiveCourseProposal($master->id());
          $catalog = $this->helper->getCatalogCourse($master->id());
          $supplemental = $node;
          $credit_hours = $master->field_crs_credit_hours->value;
          $inactive = $master->field_inactive->value;
        }
        else {
          $message = 'Cannot create required course connector from @title.';
          $values = ['@title' => $node->label()];
          $this->messenger()->addWarning($this->t($message, $values));
          $this->getLogger('hfc_req_course')->warning($message, $values);
          return FALSE;
        }
        break;

      case 'catalog_course':
        if ($master = $node->field_course_master->entity) {
          $title = $master->label();
          $proposal = $this->helper->getActiveCourseProposal($master->id());
          $catalog = $node;
          $supplemental = $this->helper->getSupplementalCourseInfo($master->id());
          $credit_hours = $master->field_crs_credit_hours->value;
          $inactive = $master->field_inactive->value;
        }
        else {
          $message = 'Cannot create required course connector from catalog course @title.';
          $values = ['@title' => $node->label()];
          $this->messenger()->addWarning($this->t($message, $values));
          $this->getLogger('hfc_req_course')->warning($message, $values);
          return FALSE;
        }
        break;
    }

    $values = [
      'title' => $title,
      'proposal_nid' => !empty($proposal) ? $proposal->id() : NULL,
      'master_nid' => !empty($master) ? $master->id() : NULL,
      'supp_nid' => !empty($supplemental) ? $supplemental->id() : NULL,
      'catalog_nid' => !empty($catalog) ? $catalog->id() : NULL,
      'credit_hours' => $credit_hours,
      'inactive' => $inactive,
    ];

    return $values;
  }

}
