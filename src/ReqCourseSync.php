<?php

namespace Drupal\hfc_req_course;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_req_course\Entity\ReqCourse;
use Drupal\node\Entity\Node;

/**
 * Defines the Required Course Connectors Sync service.
 *
 * Provides synchronization services for Required Course entities.
 *
 * @package Drupal\hfc_req_course
 */
class ReqCourseSync implements ReqCourseSyncInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Stores the Required Course Connector service.
   */
  private ReqCourseServiceInterface $rcservice;

  /**
   * Class constructor.
   *
   * @param \Drupal\hfc_req_course\ReqCourseServiceInterface $rcservice
   *   The Required Course Connector service.
   */
  public function __construct(ReqCourseServiceInterface $rcservice) {
    $this->rcservice = $rcservice;
  }

  /**
   * {@inheritdoc}
   */
  public function update(EntityInterface $node) {
    $fields = [
      'course_proposal' => 'proposal_nid',
      'course_master' => 'master_nid',
      'supplemental_course_info' => 'supp_nid',
      'catalog_course' => 'catalog_nid',
      'pseudo_course' => 'master_nid',
    ];
    switch ($node->getType()) {
      case 'course_master':
        if ($rqc = ReqCourse::lookup($fields[$node->getType()], $node->id())) {
          return $this->doUpdate($node, $rqc);
        }
        elseif ($rqc = ReqCourse::lookup('title', $node->label())) {
          return $this->doUpdate($node, $rqc);
        }
        else {
          return $this->doCreate($node);
        }
      case 'pseudo_course';
        if ($rqc = ReqCourse::lookup($fields[$node->getType()], $node->id())) {
          return $this->doUpdate($node, $rqc);
        }
        else {
          return $this->doCreate($node);
        }
      case 'course_proposal':
        if ($node->field_course_master->target_id) {
          if ($rqc = ReqCourse::lookup('master_nid', $node->field_course_master->target_id)) {
            return $this->doUpdate($node, $rqc);
          }
          else {
            if ($this->doCreate($node->field_course_master->entity)) {
              $rqc = ReqCourse::lookup('master_nid', $node->field_course_master->target_id);
              return $this->doUpdate($node, $rqc);
            }
          }
        }
        else {
          if ($rqc = ReqCourse::lookup('proposal_nid', $node->id())) {
            return $this->doUpdate($node, $rqc);
          }
          else {
            return $this->doCreate($node);
          }
        }
      case 'catalog_course':
        if ($node->field_course_master->target_id) {
          if ($rqc = ReqCourse::lookup('master_nid', $node->field_course_master->target_id)) {
            return $this->doUpdate($node, $rqc);
          }
          else {
            $this->messenger()->addMessage($this->t(
              'Cannot create Required Course Connector from Catalog Course for %t (%n)',
              ['%t' => $node->label(), '%n' => $node->id()]
            ));
            return FALSE;
          }
        }
      case 'supplemental_course_info':
        if ($node->field_course_master->target_id) {
          if ($rqc = ReqCourse::lookup('master_nid', $node->field_course_master->target_id)) {
            return $this->doUpdate($node, $rqc);
          }
          else {
            $this->messenger()->addMessage($this->t(
              'Cannot create Required Course Connector from Supplemental Course Info for %t (%n)',
              ['%t' => $node->label(), '%n' => $node->id()]
            ));
            return FALSE;
          }
        }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function nodeDelete(EntityInterface $node) {
    $fields = [
      'course_proposal' => 'proposal_nid',
      'course_master' => 'master_nid',
      'supplemental_course_info' => 'supp_nid',
      'catalog_course' => 'catalog_nid',
      'pseudo_course' => 'master_nid',
    ];
    $field = $fields[$node->getType()];
    if ($rqc = ReqCourse::lookup($field, $node->id())) {
      $rqc->set($field, 0);
      // @todo This should check to see if the connector is used in a program before removal!
      if ($rqc->getProposalNid() == 0 && $rqc->getMasterNid() == 0 && $rqc->getCatalogNid() == 0) {
        $rqc->delete();
      }
      else {
        $rqc->save();
      }
    }
  }

  /**
   * Create a new rqc record.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  private function doCreate(EntityInterface $node) {

    if ($values = $this->rcservice->getConnectorValues($node)) {
      $rqc = ReqCourse::create($values);
      return $rqc->save();
    }
  }

  /**
   * Update an existing rqc record.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The source node to read.
   * @param \Drupal\hfc_req_course\Entity\ReqCourse $rqc
   *   The rqc entity to update.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  private function doUpdate(EntityInterface $node, ReqCourse $rqc) {
    $values = $this->rcservice->getConnectorValues($node);

    if (!empty($values)) {

      $changed = FALSE;

      if ($rqc->label() !== $values['title']) {
        $rqc->setName($values['title']);
        $changed = TRUE;
      }
      if ($rqc->getProposalNid() !== $values['proposal_nid']) {
        $rqc->setProposal($values['proposal_nid']);
        $changed = TRUE;
      }
      if ($rqc->getMasterNid() !== $values['master_nid']) {
        $rqc->setMaster($values['master_nid']);
        $changed = TRUE;
      }
      if ($rqc->getSuppNid() !== $values['supp_nid']) {
        $rqc->setSupp($values['supp_nid']);
        $changed = TRUE;
      }
      if ($rqc->getCatalogNid() !== $values['catalog_nid']) {
        $rqc->setCatalog($values['catalog_nid']);
        $changed = TRUE;
      }
      if ($rqc->getCreditHours(3) !== $values['credit_hours']) {
        $rqc->setCreditHours($values['credit_hours']);
        $changed = TRUE;
      }
      if ($rqc->getInactive() !== $values['inactive']) {
        $rqc->setInactive($values['inactive']);
        $changed = TRUE;
      }

      if ($changed) {
        return $rqc->save();
      }
      else {
        return SAVED_UPDATED;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function checkBroken(array &$context) {
    $broken = \Drupal::database()->query("
      SELECT * FROM {req_course} WHERE
      (master_nid <> 0 AND master_nid NOT IN (SELECT nid FROM {node} WHERE type = 'course_master' OR type = 'pseudo_course')) OR
      (master_nid = 0 AND proposal_nid NOT IN (SELECT nid FROM {node} WHERE type = 'course_proposal')) ORDER BY title
    ")->fetchAll();

    foreach ($broken as $item) {
      $entity = ReqCourse::load($item->id);
      if (!empty($entity->getProposalNid())) {
        // Clear master and catalog if proposal is still valid.
        // @todo Make sure proposal field_course_master is not set.
        $message = 'Clearing invalid Course Master from Req Course Connector @id: @title';
        $values = ['@id' => $entity->id(), '@title' => $entity->label()];
        \Drupal::messenger()->addWarning(t($message, $values));
        \Drupal::logger('hfc_req_course')->warning($message, $values);
        $entity->setMaster(0);
        $entity->setCatalog(0);
        $entity->save();
      }
      else {
        // @todo This should check to see if the connector is used in a program before removal!
        $message = 'Removing invalid Req Course Connector @id: @title';
        $values = ['@id' => $entity->id(), '@title' => $entity->label()];
        \Drupal::messenger()->addError(t($message, $values));
        \Drupal::logger('hfc_req_course')->error($message, $values);
        $entity->delete();
      }
    }
    $context['message'] = 'Fixing broken connectors.';
    $context['results'][] = t('Found and repaired @count broken connectors.', ['@count' => count($broken)]);
  }

  /**
   * {@inheritdoc}
   */
  public static function bulkUpdate(array $nids, array &$context) {

    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($nids);
      $context['sandbox']['fail_count'] = 0;
    }

    // Batch cycle limit.
    $limit = 10;

    $slice = array_slice($nids, $context['sandbox']['progress'], $limit);
    foreach ($slice as $nid) {
      $node = Node::load($nid);
      if (\Drupal::service('hfc_req_course.sync')->update($node)) {
        $result = " - success: ($nid)";
      }
      else {
        \Drupal::messenger()->addError(t('@title - Update Failed', ['@title' => $node->label()]));
        $result = " - failed";
      }
      $context['sandbox']['progress']++;
      $context['sandbox']['current_id'] = $node->id();
      $context['message'] = Html::escape($node->label());
      $context['results'][] = $node->id() . ' : ' . Html::escape($node->label()) . $result;
      usleep(50000);
    }

    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function bulkUpdateFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = 'Process completed successfully.';
    }
    else {
      $message = 'Finished with an error.';
    }
    \Drupal::messenger()->addMessage(t($message));
  }

}
