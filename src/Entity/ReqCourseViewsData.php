<?php

namespace Drupal\hfc_req_course\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Required Course Connector entities.
 */
class ReqCourseViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['req_course']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Required Course Connector'),
      'help' => $this->t('The Required Course Connector ID.'),
    ];

    return $data;
  }

}
