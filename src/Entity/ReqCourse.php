<?php

namespace Drupal\hfc_req_course\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\hfc_req_course\ReqCourseInterface;

/**
 * Defines the Required Course Connector entity.
 *
 * @ingroup hfc_req_course
 *
 * @ContentEntityType(
 *   id = "req_course",
 *   label = @Translation("Required Course Connector"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\hfc_req_course\ReqCourseListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\hfc_req_course\Form\ReqCourseForm",
 *       "add" = "Drupal\hfc_req_course\Form\ReqCourseForm",
 *       "edit" = "Drupal\hfc_req_course\Form\ReqCourseForm",
 *       "delete" = "Drupal\hfc_req_course\Form\ReqCourseDeleteForm",
 *     },
 *     "access" = "Drupal\hfc_req_course\ReqCourseAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\hfc_req_course\ReqCourseHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "req_course",
 *   admin_permission = "administer req course",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *   },
 *   links = {
 *     "canonical" = "/req-course/{req_course}",
 *     "add-form" = "/req-course/add",
 *     "edit-form" = "/req-course/{req_course}/edit",
 *     "delete-form" = "/req-course/{req_course}/delete",
 *   },
 *   field_ui_base_route = "req_course.settings"
 * )
 */
class ReqCourse extends ContentEntityBase implements ReqCourseInterface {

  /**
   * {@inheritdoc}
   */
  public static function lookup($field, $nid) {
    $query = \Drupal::entityQuery('req_course')->condition($field, $nid);
    $ids = $query->execute();
    if (!empty($ids)) {
      return self::load(reset($ids));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('title', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProposal() {
    if (is_object($this->get('proposal_nid')->entity)) {
      return $this->get('proposal_nid')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getProposalNid() {
    if (is_object($this->get('proposal_nid')->entity)) {
      return $this->get('proposal_nid')->entity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setProposal($nid) {
    $this->set('proposal_nid', $nid);
  }

  /**
   * {@inheritdoc}
   */
  public function getMaster() {
    if (is_object($this->get('master_nid')->entity)) {
      return $this->get('master_nid')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMasterNid() {
    if (is_object($this->get('master_nid')->entity)) {
      return $this->get('master_nid')->entity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setMaster($nid) {
    $this->set('master_nid', $nid);
  }

  /**
   * {@inheritdoc}
   */
  public function getSupp() {
    if (is_object($this->get('supp_nid')->entity)) {
      return $this->get('supp_nid')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSuppNid() {
    if (is_object($this->get('supp_nid')->entity)) {
      return $this->get('supp_nid')->entity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setSupp($nid) {
    $this->set('supp_nid', $nid);
  }

  /**
   * {@inheritdoc}
   */
  public function getCatalog() {
    if (is_object($this->get('catalog_nid')->entity)) {
      return $this->get('catalog_nid')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCatalogNid() {
    if (is_object($this->get('catalog_nid')->entity)) {
      return $this->get('catalog_nid')->entity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setCatalog($nid) {
    $this->set('catalog_nid', $nid);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreditHours($scale = NULL) {
    return $scale > 0 ? number_format($this->credit_hours->value, $scale) : $this->credit_hours->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreditHours($credit_hours) {
    $this->set('credit_hours', $credit_hours);
  }

  /**
   * {@inheritdoc}
   */
  public function getInactive() {
    return $this->inactive->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInactive($inactive) {
    $this->set('inactive', $inactive);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Required Course Connector entity.'))
      ->setReadOnly(TRUE);

    $fields['proposal_nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Current Proposal'))
      ->setDescription(t('Stores the node ID of currently active proposal for the course, if applicable.'))
      ->setSetting('target_type', 'node')
      ->setSetting('max_length', 7)
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['course_proposal' => 'course_proposal']])
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'default',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['master_nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Course Master'))
      ->setDescription(t('Stores the node ID of the Course Master.'))
      ->setSetting('target_type', 'node')
      ->setSetting('max_length', 7)
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['course_master' => 'course_master']])
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'default',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['supp_nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Supplemental Course Info'))
      ->setDescription(t('Stores the node ID of the Supplemental Course Info.'))
      ->setSetting('target_type', 'node')
      ->setSetting('max_length', 7)
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['supplemental_course_info' => 'supplemental_course_info']])
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'default',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['catalog_nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Catalog Course'))
      ->setDescription(t('Stores the node ID of the catalog entry for this course.'))
      ->setSetting('target_type', 'node')
      ->setSetting('max_length', 7)
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['catalog_course' => 'catalog_course']])
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'default',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The name of the Required Course Connector entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['credit_hours'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Credit Hours'))
      ->setSettings([
        'precision' => 15,
        'scale' => 5,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['inactive'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Inactive status'))
      ->setDescription(t('A boolean indicating whether the course is inactive.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
